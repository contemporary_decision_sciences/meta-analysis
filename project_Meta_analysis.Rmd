---
title: "Meta-analysis"
author: "Tukupov_Denis"
date: "05 04 2022"
output: pdf_document
---

```{r import library'}
library(metafor)
```

```{r filter data}
dat <- dat.cannon2006[,1:6]
dat <- escalc(measure="RR", ai=ep1t, n1i=nt, ci=ep1c, n2i=nc, data=dat, slab=trial)
dat
```

```{r}
res <- rma(yi, vi, data=dat, method="DL")
res
```

```{r}
predict(res, transf=exp, digits=2) # summary effect size (risk ratio)
```

```{r}
dat$weights <- paste0(round(weights(res)), "%")   # weights in % (rounded)
dat$pvals   <- round(summary(dat)$pval, digits=3) # p-values of the individual trials
```

```{r}
par(mar=c(4,4,2,2))

forest(res, xlim=c(-1,2), atransf=exp, at=log(c(2/3, 1, 3/2)),
       header=TRUE, top=2, mlab="Summary", efac=c(0,1,3),
       ilab=data.frame(dat$weights, dat$pvals), ilab.xpos=c(0.8,1.2), ilab.pos=2)
text(0.8, -1, "100%", pos=2)
text(1.2, -1, formatC(res$pval, format="f", digits=5), pos=2)
text(0.8,  6, "Weight",  pos=2, font=2)
text(1.2,  6, "P-Value", pos=2, font=2)
```

